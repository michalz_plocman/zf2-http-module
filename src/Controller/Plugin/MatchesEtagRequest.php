<?php
namespace Http\Controller\Plugin;

use Zend\Http\Header\IfNoneMatch;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * @method AbstractActionController getController()
 */
class MatchesEtagRequest extends AbstractPlugin
{
    public function __invoke($etag)
    {
        $request = $this->getRequest();

        if (!$request instanceof Request) {
            return false;
        }

        /** @var IfNoneMatch|false $match */
        $match = $request->getHeader('If-None-Match');
        if (!$match) {
            return false;
        }

        return $match->getFieldValue() === $etag;
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->getController()->getRequest();
    }
}
