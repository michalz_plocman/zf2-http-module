<?php
namespace Http\Controller\Plugin;

use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * @method AbstractActionController getController()
 */
class SendEtag extends AbstractPlugin
{
    /**
     * @param string $etag
     * @return Response
     */
    public function __invoke($etag)
    {
        $response = $this->getResponse();
        $response->getHeaders()
            ->addHeaderLine('Cache-control', '')
            ->addHeaderLine('Pragma', '')
            ->addHeaderLine('ETag', $etag)
        ;

        return $response;
    }

    /**
     * @return Response
     */
    protected function getResponse()
    {
        return $this->getController()->getResponse();
    }
}
