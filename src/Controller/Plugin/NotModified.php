<?php
namespace Http\Controller\Plugin;

use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * @method AbstractActionController getController()
 */
class NotModified extends AbstractPlugin
{
    /**
     * @return Response
     */
    public function __invoke()
    {
        $response = $this->getResponse();
        $response->setStatusCode(Response::STATUS_CODE_304);

        return $response;
    }

    /**
     * @return Response
     */
    protected function getResponse()
    {
        return $this->getController()->getResponse();
    }
}
