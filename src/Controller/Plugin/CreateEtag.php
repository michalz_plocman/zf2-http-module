<?php
namespace Http\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class CreateEtag extends AbstractPlugin
{
    public function __invoke($value)
    {
        if (! $value) {
            // dummy key for no value
            return str_repeat('0', 32);
        }

        return md5($value);
    }
}
