<?php
namespace Http;

use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;

class Module implements ControllerPluginProviderInterface
{
    public function getControllerPluginConfig()
    {
        return [
            'invokables' => [
                'matchesEtagRequest' => Controller\Plugin\MatchesEtagRequest::class,
                'notModified'        => Controller\Plugin\NotModified::class,
                'sendEtag'           => Controller\Plugin\SendEtag::class,
                'createEtag'         => Controller\Plugin\CreateEtag::class,
            ],
        ];
    }
}
