# ZF2 Http module #

# Controller plugins

Various controller plugins are available:

**ETag handling**:

1. `createEtag(string $value): string`
2. `sendEtag(string $etag): Zend\Http\Response`
3. `matchesEtagRequest(string $etag): bool`

**Response handling**:

1. `notModified(): Zend\Http\Response`
